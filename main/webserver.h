/*
 * webserver.h
 *
 *  Created on: Dec 9, 2020
 *      Author: maria
 */

#ifndef MAIN_WEBSERVER_H_
#define MAIN_WEBSERVER_H_

#include "freertos/FreeRTOS.h"


BaseType_t ws_init();
BaseType_t ws_deinit();

#endif /* MAIN_WEBSERVER_H_ */
